FROM python:3
MAINTAINER Youhujia, Inc. Backend Awesome Team

EXPOSE 8000

RUN apt-get update && apt-get install -y mysql-client

# based on python:2.7-onbuild, but if we use that image directly
# the above apt-get line runs too late.
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
RUN pip install -i https://mirrors.aliyun.com/pypi/simple --no-cache-dir -r requirements.txt

COPY . /usr/src/app

RUN python3 manage.py migrate  --settings=$SETTING_FILE
# CMD python manager.py runserver
CMD gunicorn -b :8000 -w 4 -k gevent aixinlife.wsgi:application --env DJANGO_SETTINGS_MODULE=$SETTING_FILE
