import datetime, time

from back_end.common.exceptions import ParamsError
from back_end.common.utils.check_helper import is_int


def birthday_to_age(birthday_time=None):
    if birthday_time is None:
        return None
    time_delta = datetime.datetime.now().date() - birthday_time
    return int(time_delta.days / 365.25)


def datetime_to_timestamp(datetime=None):
    if datetime is None:
        return None
    return int(datetime.timestamp() * 1000)


def timestamp_to_datetime(timestamp):
    if not is_int(timestamp):
        raise ParamsError(msg='时间格式不正确')
    timestamp = int(timestamp)
    time_stamp = timestamp_format(timestamp)
    return datetime.datetime.fromtimestamp(time_stamp)


def timestamp_to_year_month_day(timestamp):
    time_stamp = timestamp_format(timestamp)
    date_time = timestamp_to_datetime(time_stamp)
    return date_time.year, date_time.month, date_time.day


def timestamp_to_str(timestamp, pattern='%Y-%m-%d'):
    time_stamp = timestamp_format(timestamp)
    time_local = datetime.time.localtime(time_stamp)
    return time.strftime(pattern, time_local)


def datetime_to_str(datetime, pattern='%Y-%m-%d'):
    timestamp = datetime_to_timestamp(datetime)
    return timestamp_to_str(timestamp, pattern)


def timestamp_format(time_stamp):
    if len(str(time_stamp)) == 13:
        time_stamp = int(time_stamp)
        time_stamp = int(time_stamp / 1000)
    return time_stamp


def str_to_datetime(date_time_str, pattern='%Y-%m-%d'):
    return datetime.datetime.strptime(date_time_str, pattern)


def get_next_month(date_time, days=0):
    """
    获取下个月的days的日期
    :return: 默认返回下月一号的日期
    """
    year = date_time.year
    month = date_time.month
    if month == 12:
        month = 1
        year += 1
    else:
        month += 1
    res = datetime.datetime(year, month, 1) + datetime.timedelta(days)
    return res
