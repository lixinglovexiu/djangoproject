from django.conf import settings

from back_end.common.encrypt import jwt_encode


def assemble_token(payload: dict):
    assert isinstance(payload, dict)

    return jwt_encode(payload, settings.SECRET_KEY)
