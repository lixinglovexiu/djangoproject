import base64
import os
from urllib import request

from django.http import HttpResponse
from django.utils.encoding import escape_uri_path


def remove_tmp_file(file_path):
    """删除文件"""
    if os.path.exists(file_path):
        os.remove(file_path)


def get_download_file_type(file_type):
    """根据文件类型，确定response中文件的类型"""
    if file_type == 'xls' or file_type == 'xlsx':
        file_type = 'vnd.ms-excel'
    if file_type == 'doc' or file_type == 'docx':
        file_type = 'msword'

    return file_type


def download_by_base64(base64_str, file_name):
    # 去掉字符串前的说明 data:image/jpeg;base64,
    base64_str = base64_str[base64_str.index('base64,') + len('base64,'):]
    img_data = base64.b64decode(base64_str)
    file = open(file_name, 'wb')
    file.write(img_data)
    file.close()
    return response_download(file_name)


def download_by_url(url, file_name):
    """根据url下载图片，并修改图片名称为file_name"""
    request.urlretrieve(url, file_name)
    return response_download(file_name)


def response_download(file_name):
    """在网页上使用response下载文件"""
    file_type = file_name[file_name.rindex('.') + 1:]
    download_file_type = get_download_file_type(file_type)
    f_read = open(file_name, 'rb')
    response = HttpResponse(f_read, content_type='application/' + download_file_type)
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(
        escape_uri_path(file_name))
    f_read.close()
    # 删除存在本地的临时文件
    # remove_tmp_file(file_name)

    return response


def test(request):
    # url = 'https://axhl-test-1252661357.cos.ap-shanghai.myqcloud.com' \
    #       '/avatar/1523602189375-8522cb0d-4a28-0a4e-a1d4-9347f796104f.jpg'
    # url = 'https://yhj-ax-1256661917.cos.ap-beijing.myqcloud.com' \
    #       '/ax-dev/pdf/1525922419066-f175640e-952f-65d2-d381-0e23e8245f16.pdf'
    # url = 'https://yhj-ax-1256661917.cos.ap-beijing.myqcloud.com' \
    #       '/ax-dev/docx/1525941032962-9fdbadde-d855-a36e-25fe-2d0e54036eaa.docx'
    # url = 'https://yhj-ax-1256661917.cos.ap-beijing.myqcloud.com' \
    #       '/ax-dev/xlsx/1525941055189-f855fc38-5f87-0d07-a7de-4ab5a9b76df7.xlsx'
    # return download_by_url(url, '1.xlsx')
    file_path = '/Users/lixing/Desktop/未命名.rtf'
    base64_str = open(file_path).read()

    return download_by_base64(base64_str, '1.jpeg')

# if __name__ == '__main__':
#     test()
