from back_end.common.exceptions import ParamsError
from back_end.common.utils.check_helper import is_num


def get_percent(count, total):
    # 判断是否为数字
    if is_num(count) and is_num(total):
        result = 0
        if total != 0:
            result = count / total * 100
            result = round(result, 2)
        return result
    else:
        raise ParamsError(msg='计算比例的输入不是数字')


def float_trim(float_num, count):
    """保留count位有效数字"""
    return float(('%.' + str(count) + 'f') % float_num)

