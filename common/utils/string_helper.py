def remove_last_comma(some_str):
    """如果字符串以逗号结尾，去掉逗号"""
    if some_str.endswith(','):
        return some_str.rstrip(',')
    else:
        return some_str


def str_to_json(json_str):
    """字符串转json对象"""
    if isinstance(json_str, str):
        return eval(json_str)
    else:
        return json_str


def deal_blank(obj, return_value=''):
    """处理空值情况"""
    if not obj:
        return return_value
    else:
        return obj