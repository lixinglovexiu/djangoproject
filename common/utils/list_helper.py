from functools import reduce

from back_end.common.exceptions import ParamsError


def distinct_list(list):
    # 去重，不影响顺序
    func = lambda x, y: x if y in x else x + [y]
    return reduce(func, [[], ] + list)


def get_intersection(main_list, other_list):
    """取交集"""
    if main_list is None or other_list is None:
        return []
    main_list = list(
        (set(main_list).union(set(other_list))) ^ (
                set(main_list) ^ set(other_list)))
    return main_list


def get_union(main_list, other_list):
    """取并集"""
    if main_list is None:
        main_list = []
    if other_list is None:
        other_list = []
    # 取并集
    main_list = list((set(main_list).union(set(other_list))))
    return main_list


def get_difference(main_list, other_list):
    """取差集"""
    if main_list is None:
        main_list = []
    if other_list is None:
        other_list = []
    # 取差集
    main_list = list((set(main_list) ^ (set(other_list))))
    return main_list


def remove_items_in_other_list(main_list, other_list):
    """取只包含在main_list中的值"""
    if main_list is None:
        main_list = []
    if other_list is None:
        other_list = []
    intersection_list = get_intersection(main_list, other_list)
    return get_difference(main_list, intersection_list)


def reduce_list(num_list):
    """[1,2,3]-->[1,1+2,1+2+3]"""
    result = []
    for i in range(len(num_list)):
        tmp = 0
        for j in range(i + 1):
            tmp += num_list[j]
        result.append(tmp)

    return result


def add_list(list1, list2):
    """将两个list中的元素加和"""
    # 判断list1和list2是否是list
    try:
        if not list1:
            return list2
        if isinstance(list1, list) and isinstance(list2, list):
            return [x + y for x, y in zip(list1, list2)]
        else:
            return list1 + list2
    except:
        raise ParamsError(msg='add_list方法中，传入的两个元素不能加和')
