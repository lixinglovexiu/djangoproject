def is_int(param):
    """判断是否是整数"""
    if isinstance(param, int):
        return True
    if isinstance(param, str) and param.isdigit():
        return True


def is_num(value):
    """判断是否为数字"""
    try:
        value + 1
    except TypeError:
        return False

    return True
