from types import DynamicClassAttribute
from back_end.common.enum.base import BaseEnum


class Gender(BaseEnum):
    FEMALE = (0, '女')
    MALE = (1, '男')

    @DynamicClassAttribute
    def gender(self):
        return self._value_[0]

    @DynamicClassAttribute
    def gender_name(self):
        return self._value_[1]

