from enum import Enum, unique
from types import DynamicClassAttribute


@unique
class BaseEnum(Enum):
    @classmethod
    def get_by_value(cls, value):
        for e in cls:
            if value == e.value[0]:
                return e

        return None

    @classmethod
    def get_by_name(cls, name):
        for e in cls:
            if name == e.name:
                return e

        return None

    @DynamicClassAttribute
    def db_value(self):
        return self._value_[0]

    @DynamicClassAttribute
    def desc(self):
        return self._value_[1]

