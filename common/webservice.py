from suds.client import Client


def create_client():
    """创建webservice client"""
    # 测试环境
    # url = "http://139.199.149.233:8080/sms/services/SmsVerificationMessageSendService?wsdl"
    # 正式环境
    url = "http://10.159.68.3:8080/sms/services/SmsVerificationMessageSendService?wsdl"
    client = Client(url)
    return client


def send_verification_code(phone_array, msg_array):
    """发送验证码"""
    send_message(phone_array, u'LTCVC006', msg_array, '0')


def test():
    # 发送验证码
    print('发送验证码')
    send_message([u'18610777464', ], u'LTCVC006', [u'1234', ], '0')
    # 发送模板短信
    print('发送模板短信')
    send_message([u'18610777464', ], u'LTCLTA002', [u'李兴', '符合'], '1')


def send_message(phone_array, service_code, msg_array, send_type):
    """
    发送短信息
    phone_array：要发送的电话号码数组
    service_code：服务id
    msg_array： 消息数组
    send_type： 短信类型 '0'： 验证码短信， '1'： 普通短信
    """
    client = create_client()
    phone_nums = client.factory.create('ns1:Array')
    phone_nums.item = phone_array

    msg = client.factory.create('ns1:Array')
    msg.item = msg_array

    result = client.service.SmsVerificationMessageSend(
        cMobileNums=phone_nums,
        cServiceCode=service_code,
        cSendData=msg,
        cUserCode=u'LTCI',
        cPassWord=u'1788922DA80081CA',
        cSystemCode=u'LTC',
        cDataType=u'0',
        cMessageCode=None,
        cSendType=send_type
    )
    print(result)


if __name__ == '__main__':
    test()
