import xlwt
from django.http import HttpResponse
from django.utils.encoding import escape_uri_path
from xlwt import Borders


def test():
    wbk = xlwt.Workbook()
    sheet = wbk.add_sheet('评估表')
    # 合并单元格
    sheet.write_merge(0, 0, 0, 4, '标题', style_title())

    subheading = ['姓名', '年龄', '性别', '身份证', '婚否']

    for i, heading in enumerate(subheading):
        sheet.write(1, i, heading, style_subheading())

    wbk.save('/Users/lixing/Desktop/1.xls')

    # return '', wbk


def write_line(sheet, row_num, content_list):
    for i, content in enumerate(content_list):
        sheet.write(row_num, i, content, style_content())  # 序号


def style_borders():
    """边框"""
    borders = Borders()
    borders.left = Borders.THIN
    borders.right = Borders.THIN
    borders.top = Borders.THIN
    borders.bottom = Borders.THIN
    return borders


def style_font(height, bold=False):
    """字体"""
    font = xlwt.Font()
    font.name = '宋体-简'
    font.bold = bold
    font.height = height
    return font


def align_left():
    """左对齐方式"""
    alignment = xlwt.Alignment()
    alignment.horz = xlwt.Alignment.HORZ_LEFT
    alignment.vert = xlwt.Alignment.VERT_CENTER
    alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT
    return alignment


def align_center():
    """居中对齐方式"""
    alignment = xlwt.Alignment()
    alignment.horz = xlwt.Alignment.HORZ_CENTER
    alignment.vert = xlwt.Alignment.VERT_CENTER
    alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT
    return alignment


def align_right():
    """右对齐方式"""
    alignment = xlwt.Alignment()
    alignment.horz = xlwt.Alignment.HORZ_RIGHT
    alignment.vert = xlwt.Alignment.VERT_CENTER
    alignment.wrap = xlwt.Alignment.WRAP_AT_RIGHT
    return alignment


def style_title(borders=False):
    """标题样式"""
    style = xlwt.XFStyle()
    # 字体
    font = style_font(440, bold=True)

    # 对齐方式
    alignment = align_center()

    if borders:
        borders = style_borders()
        style.borders = borders

    style.font = font
    style.alignment = alignment

    return style


def style_subheading(borders=True):
    """副标题样式"""
    style = xlwt.XFStyle()

    # 字体
    font = style_font(320, bold=True)

    # 对齐方式
    alignment = align_center()

    # 边框
    if borders:
        borders = style_borders()
        style.borders = borders

    style.font = font
    style.alignment = alignment

    return style


def style_content(alignment=align_center(), borders=True):
    # 正文样式
    style = xlwt.XFStyle()
    # 边框
    if borders:
        borders = style_borders()
        style.borders = borders

    style.alignment = alignment

    return style


def write_content(sheet, r, c, content, style=style_content()):
    sheet.write(r, c, content, style)


def out_file(file_name, document):
    # file_name = '中文.doc'.encode('utf-8')

    response = HttpResponse()
    # 允许跨域访问
    response['Access-Control-Allow-Origin'] = '*'
    response['Content-Type'] = 'application/vnd.ms-excel'  # 文件类型
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(
        escape_uri_path(file_name + '.xls'))

    document.save(response)
    return response


if __name__ == '__main__':
    test()
