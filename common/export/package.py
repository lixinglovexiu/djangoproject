import zipfile

from django.http import HttpResponse
from django.utils.encoding import escape_uri_path

from back_end.common import remove_tmp_file


def zip_download_remove_files(file_names, zip_name):
    """压缩，网页下载，移除源文件"""
    f = zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED)

    file_count = len(file_names)
    for i in range(file_count):
        xls_path = file_names[i]
        # 根据文件路径打包zip
        f.write(xls_path)
        remove_tmp_file(xls_path)

    f.close()
    f_read = open(f.filename, 'rb')
    response = HttpResponse(f_read, content_type='application/zip')
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(
        escape_uri_path(zip_name))
    f_read.close()

    remove_tmp_file(zip_name)
    return response



