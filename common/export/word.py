import docx

import requests
from django.http import HttpResponse
from django.utils.encoding import escape_uri_path
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import *
from docx.oxml.ns import *
from docx.shared import Pt, Inches, RGBColor

from back_end.common.utils import file_helper


def test(request):
    document = docx.Document()

    title_style = get_title_style(document)
    head = document.add_paragraph('石景山区长期护理保险失能复评结论书', style=title_style)
    # 设置段落水平居中对齐
    head.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER

    body_style = get_body_style(document)
    document.add_paragraph('', style=body_style)
    sub_head = document.add_paragraph('号', style=body_style)
    # 设置段落水平右对齐
    sub_head.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT
    document.add_paragraph('', style=body_style)

    document.add_paragraph("被评估人", style=body_style)
    document.add_paragraph("身份证号", style=body_style)
    document.add_paragraph("居住地址", style=body_style)
    document.add_paragraph('', style=body_style)
    document.add_paragraph("根据石景山区长期护理保险失能评估委员会专家评估，您目前的失能复评结论为：", style=body_style)
    document.add_paragraph("本评估结论为最终结论。", style=body_style)
    document.add_paragraph('', style=body_style)
    document.add_paragraph('', style=body_style)
    document.add_paragraph('', style=body_style)

    sign_style = get_sign_style(document)
    sign = document.add_paragraph("石景山区长期护理保险失能评估委员会", style=sign_style)
    # 设置段落水平右对齐
    sign.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT

    time_style = get_time_style(document)
    time = document.add_paragraph("年   月   日", style=time_style)
    # 设置段落水平右对齐
    time.paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.RIGHT

    # 保存
    document.save("/Users/hy/Desktop/writeResult.docx")


def out_file(file_name, document):
    # file_name = '中文.doc'.encode('utf-8')
    response = HttpResponse()
    # 允许跨域访问
    response['Access-Control-Allow-Origin'] = '*'
    response['Content-Type'] = 'application/msword'  # 文件类型
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(escape_uri_path(file_name))

    document.save(response)
    return response


def download_file(url, file_name):
    r = requests.get(url, stream=True)
    f = open(file_name, 'wb')
    for chunk in r.iter_content(chunk_size=512):
        if chunk:
            f.write(chunk)
    f.close()

    f_read = open(file_name, 'rb')
    response = HttpResponse(f_read, content_type='application/msword')
    response['Content-Disposition'] = "attachment; filename*=utf-8''{}".format(
        escape_uri_path(file_name))
    f_read.close()
    # 删除存在本地的临时文件
    file_helper.remove_tmp_file(file_name)

    return response


def get_title_style(document, font_size=20):
    """标题样式"""
    title_style = document.styles.add_style('lixing_title', WD_STYLE_TYPE.PARAGRAPH)
    title_font = title_style.font
    title_font.name = 'SimSun'
    title_font.size = Pt(font_size)
    title_font.bold = True
    title_font.color.rgb = black()

    return title_style


def get_body_style(document, font_size=16, left_indent=20, first_line_indent=True):
    """正文样式"""
    style = document.styles.add_style('lixing_body', WD_STYLE_TYPE.PARAGRAPH)
    font = style.font

    # 设置样式中的字符属性 ，操作方法和上面改变内联对象属性方法一致
    font.name = u'FangSong'
    style._element.rPr.rFonts.set(qn('w:eastAsia'), u'FangSong')
    font.size = Pt(font_size)

    para_format = style.paragraph_format
    para_format.left_indent = Pt(left_indent)
    if first_line_indent:
        para_format.first_line_indent = Inches(0.5)
    para_format.widow_control = True

    return style


def get_sign_style(document):
    """签名样式"""
    style = document.styles['Body Text']
    font = style.font

    # 设置样式中的字符属性 ，操作方法和上面改变内联对象属性方法一致
    font.name = u'FangSong'
    style._element.rPr.rFonts.set(qn('w:eastAsia'), u'FangSong')
    font.size = Pt(16)
    font.color.rgb = red()

    para_format = style.paragraph_format
    para_format.right_indent = Pt(20)
    para_format.widow_control = True

    return style


def get_time_style(document):
    """时间戳样式"""
    style = document.styles['Body Text 2']
    font = style.font

    # 设置样式中的字符属性 ，操作方法和上面改变内联对象属性方法一致
    font.name = u'FangSong'
    style._element.rPr.rFonts.set(qn('w:eastAsia'), u'FangSong')
    font.size = Pt(16)

    para_format = style.paragraph_format
    para_format.right_indent = Pt(20)
    para_format.widow_control = True

    return style


def black():
    """黑色"""
    return RGBColor(0, 0, 0)


def red():
    """红色"""
    return RGBColor(255, 0, 0)


def set_align(paragraphs, align=WD_PARAGRAPH_ALIGNMENT.CENTER):
    """设置段落格式, 默认居中"""
    for paragraph in paragraphs:
        paragraph.paragraph_format.alignment = align


if __name__ == '__main__':
    test()
