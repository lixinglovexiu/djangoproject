import logging

import requests


def send_captcha_via_servicer(phone: str, msg: str):
    """
    发送短信验证码
    调用第三方服务，发送业务短信
    """
    assert phone
    assert msg
    logging.info('Send phone msg, phone: {0}, msg: {1}'.format(phone, msg))

    url = 'https://sms.yunpian.com/v2/sms/single_send.json'
    data = {'apikey': '134755ebcead3612d16f43a7339627c6', 'mobile': phone, 'text': msg, }
    response = requests.post(url, data)

    if response.status_code is 200 and response.json().get('code') is 0:
        return True

    logging.error('Send sms fail, status code: {0}, response: {1}'.format(response.status_code, response.text))


#


