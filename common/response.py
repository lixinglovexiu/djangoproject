from django.http import JsonResponse, HttpResponse


def set_headers(response):
    response['Access-Control-Allow-Origin'] = '*'
    response['Access-Control-Allow-Headers'] = 'Access-Control-Allow-Headers' + \
                                               ',Access-Control-Allow-Methods' + \
                                               ',Access-Control-Allow-Origin' + \
                                               ',Authorization' + \
                                               ',Content-Type' + \
                                               ',Crossdomain'
    response['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE, PATCH, OPTIONS'
    response['Access-Control-Allow-Credentials'] = True
    return response


def ok(data=None):
    response = JsonResponse({
        'result': {
            'success': True,
            'code': 0,
            'msg': '',
            'displaymsg': '',
        },
        'data': data if data else {}
    })

    response = set_headers(response)
    return response


def options_ok():
    response = HttpResponse()
    response = set_headers(response)
    return response


def method_error():
    return JsonResponse({
        'result': {
            'success': False,
            'code': 405,
            'msg': 'Method Not Allowed',
            'displaymsg': 'Method Not Allowed',
        },
        'data': {}
    })


def error_response(code=500, msg=None, displaymsg=None):
    response = JsonResponse({
        'result': {
            'success': False,
            'code': code,
            'msg': msg if msg else '',
            'displaymsg': displaymsg if displaymsg else '',
        },
        'data': {}
    })
    response = set_headers(response)
    return response


def not_authorized(msg=None):
    response = JsonResponse({
        'result': {
            'success': False,
            'code': 401,
            'msg': msg or '请登录',
            'displaymsg': msg or '请登录',
        },
        'data': {}
    })
    response = set_headers(response)
    return response
