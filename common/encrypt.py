import datetime
import json
import random
import string

import jwt

JWT_ALG = 'HS256'
SIX_MONTH_SECONDS = 6 * 30 * 24 * 3600


def jwt_encode(payload: dict, secret: str, expired: int = None):
    """
    JWT 序列化
    :param payload: 需要被加密的 dict 类型的数据
    :param secret: Secret
    :param expired: 过期时间，单位: 秒, 默认为 6 个月
    :return: JWT 序列化的数据
    """
    assert isinstance(payload, dict)
    assert isinstance(secret, str)
    assert isinstance(expired, (int, type(None)))

    if not secret:
        raise Exception('missing secert')

    return jwt.encode({
        'exp': datetime.datetime.utcnow() + datetime.timedelta(seconds=expired or SIX_MONTH_SECONDS),
        'payload': json.dumps(payload),
    }, secret, algorithm=JWT_ALG).decode("utf-8")


def jwt_decode(jwt_str: str, secret: str):
    """
    JWT  解析
    :param jwt_str: JWT 序列化的数据
    :param secret: Secret
    :return: 解析出的数据
    """
    assert isinstance(jwt_str, str)
    assert isinstance(secret, str)

    try:
        decoded = jwt.decode(jwt_str, secret, algorithms=[JWT_ALG])
        return json.loads(decoded.get('payload'))
    except Exception as e:
        return None


def random_str(length: int):
    """随机字符串，字母+数字"""
    return ''.join(random.choices(string.ascii_letters + string.digits, k=length))


def random_number_str(length: int):
    """随机字符串：数字"""
    return ''.join(random.choices(string.digits, k=length))
