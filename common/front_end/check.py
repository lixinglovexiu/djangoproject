from back_end.common import utils
from back_end.common.exceptions import ParamsError


def check_not_blank(body, param_name, err_msg):
    """校验非空"""
    param_name = body.get(param_name)

    if not param_name:
        raise ParamsError(msg=err_msg)


def check_required_param(body, param_name, err_msg='必填项未填写'):
    """校验必填项"""
    param_name = body.get(param_name)

    if isinstance(param_name, int):
        return

    if not param_name:
        raise ParamsError(msg=err_msg)


def check_is_int(body, param_name, err_msg='请填写整数'):
    """校验是否是整数"""
    param_name = body.get(param_name)

    if utils.is_int(param_name):
        return
    raise ParamsError(msg=err_msg)

