from django.db import models


class BaseModel(models.Model):
    id = models.AutoField(primary_key=True)
    is_deleted = models.BigIntegerField(verbose_name='删除标记', default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def get_by_id(cls, id):
        try:
            obj = cls.objects.get(id=id)
            if obj.is_deleted:
                return None
            return obj
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_all(cls):
        return cls.objects.filter(is_deleted=0)

    def soft_delete(self):
        self.is_deleted = 1
        self.save()

    class Meta:
        abstract = True
